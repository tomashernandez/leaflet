import React, { useEffect, useState, useMemo } from "react";
import {
  MapContainer,
  TileLayer,
  Marker,
  Popup,
  Tooltip,
  CircleMarker,
  Polygon,
  Circle,
  Rectangle,
} from "react-leaflet";
import "./App.css";
import "leaflet/dist/leaflet.css";

function App() {
  const [state, setState] = useState({
    longitude: 16.86148706364665,
    latitude: -99.86715487042328,
  });
  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        setState({
          longitude: position?.coords?.longitude,
          latitude: position?.coords?.latitude,
        });
      },
      function (error) {
        console.log("Error", error);
      },
      {
        enableHighAccuracy: true,
      }
    );
  }, []);

  console.log("state :>> ", state);

  const multiPolygon = [
    [
      [16.86148706364665, -99.91762179034657],
      [16.86148706364665, -99.89511905616246],
      [16.95, -99.79511905616246],
    ],
  ];

  const rectangle = [
    [16.881, -99.96],
    [16.91, -99.86],
  ];

  function TooltipCircle() {
    const [clickedCount, setClickedCount] = useState(0);
    const eventHandlers = useMemo(
      () => ({
        click() {
          setClickedCount((count) => count + 1);
        },
      }),
      []
    );

    const clickedText =
      clickedCount === 0
        ? "Click this Circle to change the Tooltip text"
        : `Circle click: ${clickedCount}`;

    return (
      <Circle
        center={[state.longitude, state.latitude]}
        eventHandlers={eventHandlers}
        pathOptions={{ fillColor: "blue" }}
        radius={200}
      >
        <Tooltip>{clickedText}</Tooltip>
      </Circle>
    );
  }

  return (
    <div>
      <h2>Longitude: {state.longitude}</h2>
      <h2>Latitude: {state.latitude}</h2>
      <MapContainer
        style={{ height: "650px", width: "100%" }}
        center={[state.longitude, state.latitude]}
        zoom={13}
        scrollWheelZoom={true}
      >
        <TooltipCircle />
        <CircleMarker
          center={[16.86148706364665, -99.86]}
          pathOptions={{ color: "red" }}
          radius={20}
        >
          <Tooltip>Tooltip para el circulo</Tooltip>
        </CircleMarker>
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Polygon pathOptions={{ color: "purple" }} positions={multiPolygon}>
          <Tooltip sticky>Otro Tooltip sobre el polígono</Tooltip>
        </Polygon>
        <Rectangle bounds={rectangle} pathOptions={{ color: "black" }}>
          <Tooltip direction="bottom" offset={[0, 20]} opacity={1} permanent>
            Permanente tooltip
          </Tooltip>
          <Marker position={[16.86148706364665, -99.91762179034657]}>
            <Popup>PopUp</Popup>
          </Marker>
        </Rectangle>
      </MapContainer>
    </div>
  );
}

export default App;
